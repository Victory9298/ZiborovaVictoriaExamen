import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.util.Scanner;

public class Connection {
    private static java.sql.Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String url = "jdbc:mysql://localhost:3310/exam";
    private static String user = "root", pass = "root";
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, user, pass);
            System.out.println("Hello! Welcome to our application!");
            Boolean answer = true;
            statement = connect.createStatement();
            while (answer) {
                askForDatat();
                System.out.println("Great job! Here are all students inserted: ");
                resultSet = statement.executeQuery("select * from people");
                while (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    String name = resultSet.getString(2);
                    String surname = resultSet.getString(3);
                    String sex = resultSet.getString(4);
                    String facultate = resultSet.getString(5);
                    String age = resultSet.getString(6);
                    System.out.println(id + " " + name + " " + surname + " " + facultate + " " + sex + " " + age);
                }
                System.out.println("Do you want to add one more student? Y/N");
                String line = sc.nextLine();
                if (!line.toLowerCase().equals("y")) {
                    answer = false;
                }
            }
            System.out.println("Great job! See you soon! Bye!");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace(); }
    }

    public static void askForDatat() throws SQLException {

        System.out.println("Please enter name: ");
        String nume = sc.nextLine();
        System.out.println("Please enter last name: ");
        String prenume = sc.nextLine();
        System.out.println("Please enter sex (M/F): ");
        String sex = sc.nextLine();
        System.out.println("Please enter faculty: ");
        String facultate = sc.nextLine();
        System.out.println("Please enter age: ");
        String age = sc.nextLine();

        insertValues(nume, prenume, age, facultate, sex);
    }

    public static void insertValues(String nume, String prenume, String age, String facultate, String sex) throws SQLException {

        PreparedStatement stmt = connect.prepareStatement("INSERT INTO people (nume, prenume, age, facultate, sex) VALUES (?, ?, ?, ?, ?)");

        stmt.setString(1, nume);
        stmt.setString(2, prenume);
        stmt.setString(3, age);
        stmt.setString(4, facultate);
        stmt.setString(5,  sex);
        stmt.executeUpdate();
        stmt.close();

    }
}
